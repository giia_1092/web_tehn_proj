import React from 'react';
import './App.css';
import ListItems from './ListItems'
import FriendsList from './FriendsList'
import { BrowserRouter as Router, NavLink,} from 'react-router-dom';
import Route from 'react-router-dom/Route';
import logo from './media/logo.jpg'
import meat from './media/meat.jpg'
import vegetables from './media/vegetables.jpg'
import diaryProducts from './media/diaryProducts.jpg'
import fruits from './media/fruits.jpg'
import axios from 'axios'

var localServer="http://13.59.92.62:8080/api/food/";
var homePage="http://13.59.92.62:3000/Home/";


class App extends React.Component{
    
    
    constructor(props){
      super(props);
      this.state={
        
        //incarcate din baza de date
        products:[],
        //produsele pe categorii
        meat:[],
        dairyProducts:[],
        vegetables:[],
        fruits:[],
        
        currentItem:{
          key: '',
          foodName: "",
          category: "",
          expireDate: "",
          
        },
        
         currentFriend:{
            name:'',
            category:'',
            key:''
        },
        friends:[]
      }
      this.handleInput=this.handleInput.bind(this);
      this.addItem=this.addItem.bind(this);
      this.deleteItem=this.deleteItem.bind(this);
      this.setUpdate=this.setUpdate.bind(this);
      this.addFriend=this.addFriend.bind(this);
      this.handleFriend=this.handleFriend.bind(this);
      
    }
    
    handleInput(e){
      this.setState({
          currentItem:{
          key:Date.now()%10000,
          foodName:document.getElementById("foodName").value,
          category: document.getElementById("select").options[document.getElementById("select").selectedIndex].text,
          expireDate:document.getElementById("expireDate").value,
          
          
        },
      })
    }
    
       handleFriend(){
       this.setState({
           currentFriend:{
               name:document.getElementById("friendName").value,
               category:document.getElementById("friendType").value,
               key:Date.now()
           }
       })
    }
 
  
    //import din baza de date
     componentDidMount ()  {
      
     axios.get(localServer).then(products => {
      
      var almostExpired=0;
       for(let i=0;i<products.data.length;i++)
       {
        
           products.data[i].expireDate=products.data[i].expireDate.slice(0, 10)
           
           if(products.data[i].category==="Meat"){
           this.state.meat.push(products.data[i]);
           }
           
           if(products.data[i].category==="DairyProducts"){
           this.state.dairyProducts.push(products.data[i]);
           }
           
           if(products.data[i].category==="Fruits"){
           this.state.fruits.push(products.data[i]);
           }
           
            if(products.data[i].category==="Vegetables"){
           this.state.vegetables.push(products.data[i]);
            }
           
           
           var today = new Date();
           var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+(today.getDate()-3);
           
           if(products.data[i].expireDate<date);
           almostExpired++;
       }
       if(almostExpired>0)
       alert("Careful you have products that are about to expire");
       
    })
    
     }

    getbyId(key){
      var a=localServer+key;
      axios.get(a).then(products => {
      this.setState({
        products: products.data
      })
    })
    return this.products;
    }
    
    deleteById(key){
      var a=localServer+key;
      axios.delete(a);
    }
   
   
    addItem(e){
      
    
    e.preventDefault();
    
   
    
     //check what is selected
    var select=document.getElementById("select");
    var selected=select.options[select.selectedIndex].text;
    //adaug noul item din state
    const newItem=this.state.currentItem;
   
    //adaug noul item in baza de date
    axios.post(localServer,newItem);

    console.log(newItem);
    
     //Add meat to Meat List
    if(selected==="Meat"){
        if(newItem.text!==""){
        const newItems=[...this.state.meat,newItem];
        this.setState({
          meat:newItems,
          currentItem:{
            key:0 ,
            foodName: '',
            category: '',
            expireDate: "",
           
            
          }
        })
      }
     }
     
     //add diary product to Diary Products list
     if(selected==="DairyProducts"){
        if(newItem.text!==""){
        const newItems=[...this.state.dairyProducts,newItem];
        this.setState({
          dairyProducts:newItems,
          currentItem:{
              key:0 ,
              foodName: '',
              category: '',
              expireDate: "",
              
            
          }
        })
      }
      
     }
     
     //add vegetable to Vagetables list
     if(selected==="Vegetables"){
        if(newItem.text!==""){
        const newItems=[...this.state.vegetables,newItem];
        this.setState({
          vegetables:newItems,
          currentItem:{
              key:0 ,
              foodName: '',
              category: '',
              expireDate: "",
              
            
          }
        })
      }
     }
     
     //add fruit to Fruits list
     if(selected==="Fruits"){
        if(newItem.text!==""){
        const newItems=[...this.state.fruits,newItem];
        this.setState({
          fruits:newItems,
          currentItem:{
              key:0 ,
              foodName: '',
              category: '',
              expireDate: "",
              
            
          }
        })
      }
     }
     
    }
    
   deleteItem(key){
     //delete from database;
      this.deleteById(key);
      
     //delete item from MeatList
      const filteredmeat= this.state.meat.filter(item =>
      item.key!==key);
       this.setState({
      meat: filteredmeat
      })
    
     //delete item from Diary Producs
      const filtereddairyProducts= this.state.dairyProducts.filter(item =>
      item.key!==key);
       this.setState({
      dairyProducts: filtereddairyProducts
      })
      
     //delete item from Vegetable
       const filteredVegetables= this.state.vegetables.filter(item =>
      item.key!==key);
       this.setState({
      vegetables: filteredVegetables
      })
      
      //delete item from Fruits
       const filteredFruits= this.state.fruits.filter(item =>
      item.key!==key);
       this.setState({
      fruits: filteredFruits
      })
    
     
   }
   
   setUpdate(text,key){
    
    //update item from Meat
     const meat1=this.state.meat;
     meat1.map(item=>{
       if(item.key===key){
         item.foodName=text;
       }
     })
         this.setState({
      meat: meat1
    })
    
    //update item from Diary Products
    const dairyProducts1=this.state.dairyProducts;
      dairyProducts1.map(item=>{
       if(item.key===key){
         item.foodName=text;
       }
     })
         this.setState({
      dairyProducts: dairyProducts1
    })
    
     //update item from Vegetable
     const vegetables1=this.state.vegetables;
      vegetables1.map(item=>{
       if(item.key===key){
        item.foodName=text;
       }
     })
         this.setState({
      vegetables: vegetables1
    })
    
    //update item from Fruits
     const fruits1=this.state.fruits;
      fruits1.map(item=>{
       if(item.key===key){
         item.foodName=text;
       }
     })
         this.setState({
      fruits: fruits1
    })
    
   }

    addFriend(e){
    
      
      e.preventDefault();
      const newItem=this.state.currentFriend;
      

        if(newItem.name!==""){
        const newItems=[...this.state.friends,newItem];
        this.setState({
          friends:newItems,
          currentFriend:{
             name:"",
             key:'',
             category:""
          }
        })
      }
    
    }
    openSite(){
        var user=document.getElementById("username").value;
        var password=document.getElementById("password").value;
        
        
        if(user && password)
        window.open(homePage, '_self');
        else 
        alert("You must complete both fileds");
        
    }
  render(){
       
    return(
      
     <Router>

        <Route path="/Login/" exact strict render={
          () => {
             return(
             
          <div id="login">
          <img id="logo" src={logo}/>
           <div id="userinfo">
             
             <label>Username</label>
             <input type="text" id="username"/>
             <h1></h1>
             <label>Password</label>
             <input type="password" id="password"/>
            <button onClick={this.openSite}>Login</button>
            </div>
          </div>
        )
    
        }
          }
        />

      <Route path='/home/' exact strict render={
      ()=>{
           return(
           
           <div className='App'   >
      <h1 id="usenameHI">Hi! Remeber don't waste your food!</h1>
      <img id="logo" src={logo} height="100" width="100"/>
      
       <header>
       
        <form id="to-do-form"  onSubmit={this.addItem} >
        
           <h1>What do you have in your frigde?</h1>
           
          <input type="text" placeholder="Enter Text" id="foodName"
          /> 
          
          <select name="Category" id="select">
                <option value="Meat">Meat</option>
                <option value="Dairy Products">DairyProducts</option>
                <option value="Vegetables">Vegetables</option>
                <option value="Fruits">Fruits</option>
            </select>
          <input type="date" name="expireDate" defaultValue="2010-10-10" id="expireDate"/>
          
          <button type="submit" onClick={this.handleInput}>Add</button>
        </form>
     
      </header>
      
      
      <h2>Meat</h2>
      <img id="meat" src={meat} height="100" width="100"/>
      <ListItems items={this.state.meat} 
      deleteItem={this.deleteItem}
      setUpdate={this.setUpdate}></ListItems>
      
      <h2>Dairy Products</h2>
      <img id="diaryProducts" src={diaryProducts} height="100" width="100"/>
      <ListItems items={this.state.dairyProducts} 
      deleteItem={this.deleteItem}
      setUpdate={this.setUpdate}></ListItems>
      
       <h2>Vegetables</h2>
        <img id="vegetables" src={vegetables} height="100" width="100"/>
      <ListItems items={this.state.vegetables} 
      deleteItem={this.deleteItem}
      setUpdate={this.setUpdate}></ListItems>
      
       <h2>Fruits</h2>
       <img id="fruits" src={fruits} height="100" width="100"/>
      <ListItems items={this.state.fruits} 
      deleteItem={this.deleteItem}
      setUpdate={this.setUpdate}></ListItems>
      
      
      <div id="friends">
        <form onSubmit={this.addFriend}>
             <h1 id="friendsList">Friends list:</h1>
             <input id="friendName"
             type="text"
             />
             <input type="text"
             id="friendType"
             />
             <button type="submit" onClick={this.handleFriend}>Add Friend</button>
            <FriendsList friends={this.state.friends}/>
        </form>
      </div>
      
     
       </div>
      
      
      )}}/>
      
      
      </Router>
      )
      
  }
}


export default App;
