import React from 'react';
import "./ListItems.css";


function ListItems(props){
    const items=props.items;
        const listItems=items.map(item=>
            {
                return <div className="list1" key={item.key}>
                   
                    <p>
                        <input type="text" 
                        id={item.key} 
                        value={item.foodName}
                        onChange={
                            (e)=>{
                                props.setUpdate(e.target.value,item.key);
                            }
                        }
                        />
                        <input type="text" 
                        id={item.key+1} 
                        value={item.category}
                        readOnly
                        />
                        <input type="date" 
                        id={item.key} 
                        value={item.expireDate}
                        readOnly
                        />
                        <input type="text" 
                        id={item.key} 
                        value="Donate"
                        readOnly
                        />
                        
                        
                         
                         <button id="Delete" onClick={ ()=> props.deleteItem(item.key)}>
                        Delete</button>
                        <input type="checkbox" id="check"/>
                       
                    
                    </p>
                </div>
        
             })
    return(
        <div>{listItems}</div>
    )
}

export default  ListItems;