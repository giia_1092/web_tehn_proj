const express = require('express');
const router = express.Router();

const { createFood, getAllFood, getFoodById, deleteFoodById} = require('./../controller/food');



router.post('/food', createFood);
router.get('/food', getAllFood);
router.get('/food/:id', getFoodById);
router.delete('/food/:id', deleteFoodById);








module.exports = router;