const { Food } = require('./../models/food');


const food = {
    create: async (food) => {
        try {
            const result = await Food.create(food);
            return result;    
        } catch(err) {
           throw new Error(err.message); 
        }
    },
    getAll: async () => {
        try {
            const foods = await Food.findAll();
            return foods;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    getFoodById: async (id) => {
        try {
            const food= await Food.findOne({where:{key:id} } );
            return food;
        } catch(err) {
            throw new Error(err.message);
        }
    },
    
    deleteByID: async (id) => {
        try {
            let food= await Food.findOne({where:{key:id} } );
            await food.destroy()
        } catch(err) {
            throw new Error(err.message);
        }
    }
    
}

module.exports = food;