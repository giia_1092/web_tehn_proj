const foodService = require('./../service/food');

const createFood = async (req, res, next) => {
    const food = req.body;
    if(food.foodName && food.category && food.expireDate) {
        const result = await foodService.create(food);
        res.status(201).send({
            message: 'Food added successfully.'
        });
    } else {
        res.status(400).send({
            message: 'Invalid food'
        });
    }
}

const getAllFood = async (req, res, next) => {
    try {
        const food = await foodService.getAll();
        res.status(200).send(food);
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        });
    }
}


const getFoodById = async (req, res, next) => {
    try {
        const id=req.params.id;
        if(id) {
            try {
                const food = await foodService.getFoodById(id);
                res.status(200).send(food);
            } catch(err) {
                res.status(500).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(400).send({
                message: 'No id specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

const deleteFoodById = async (req, res, next) => {
    try {
        const id=req.params.id;
        if(id) {
            try {
                const food = await foodService.deleteByID(id);
                res.status(202).send({message : 'accepted'});
            } catch(err) {
                res.status(501).send({
                    message: `Err occured: ${err.message}`
                })
            }
        } else {
            res.status(404).send({
                message: 'No id specified'
            })
        }
    } catch(err) {
        res.status(500).send({
            message: `Error occured: ${err.message}`
        })
    }
}

   


module.exports = {
    createFood,
    getAllFood,
    getFoodById,
    deleteFoodById
}