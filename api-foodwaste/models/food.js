const configuration  = require('./../config/configuration.json');
const Sequelize = require('sequelize');

const DB_NAME = configuration.database.database_name;
const DB_USER = configuration.database.username;
const DB_PASS = configuration.database.password;

const sequelize = new Sequelize(DB_NAME, DB_USER, DB_PASS, {
    dialect: 'mysql'
});

sequelize.authenticate().then(() => {
    console.log('Database connection success!');
}).catch(err => {
    console.log(`Database connection error: ${err}`);
});


class Food extends Sequelize.Model { };

Food.init({
    key: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true
    },
    foodName: {
        type: Sequelize.STRING,
        allowNull: false
    },
    category: {
        type: Sequelize.STRING,
        allowNull: false
    },
    expireDate: {
        type: Sequelize.DATE,
        allowNull: false
    }
}, {
    sequelize,
    modelName: 'food'
});

Food.sync({force: true});

// class userAccounts extends Sequelize.Model { };

// userAccounts.init({
//     key: {
//         type: Sequelize.INTEGER,
//         primaryKey: true,
//         autoIncrement: true
//     },
//     userName: {
//         type: Sequelize.STRING,
//         allowNull: false
//     },
//     password: {
//         type: Sequelize.STRING,
//         allowNull: false
//     },
// }, {
//     sequelize,
//     modelName: 'users'
// });

// userAccounts.sync({force: true});

module.exports = {
    sequelize,
    Food,
    //userAccounts
}